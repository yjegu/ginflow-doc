.. GinFlow-Doc documentation master file, created by
   sphinx-quickstart on Thu Jun  4 16:35:42 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to GinFlow's documentation!
=======================================

.. toctree::
   :maxdepth: 2
   
   background/index
   user/getting-started
   user/index
   developper/index
   graphical_interface/index


..
   Indices and tables
   ==================
   * :ref:`genindex`
   * :ref:`modindex`
   * :ref:`search`

