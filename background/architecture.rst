Architecture
============

The GinFlow architecture is composed of three layers, illustrated on the figure
below :

.. image:: ../images/ginflow-archi.png
    :align: center
    :width: 50%

The GinFlow architecture comprises three layers.

* The top layer is the user interface. It comes in two flavours allowing
  you to specify your workflow either using a simple JSON file or
  programmatically using the Java interface. 

* The middle layer is the *executor*  It is the GinFlow layer which is
  responsible for the actual deployment of your workflow on your infrastructure.
  Three executors are currently proposed:
  
  * The *centralised executor* starts your workflow in a centralised mode where
    a single HOCL interpreter is started, fed with your workflow's description and
    will be responsible for the whole execution, the satisfaction of data
    dependencies and the actual calls to the commands realising the tasks.

  * The *SSH executor* deploys your workflow on a set of worker nodes and fills
    the initial shared space with your workflow description. It relies on SSH
    connections to start and configure the agents. Once the agents are started and
    the shared space is initialised, the workflow execution is autonomous.

  * The *Mesos executor* allows you to use your own Mesos instance managing your 
    computing nodes. It is interfaced with the Mesos primitives to start and control
    the status of the agents.

* The bottow layer is the core of GinFlow and implements the workflow agents
  through a set of HOCL interpreters and a shared space containing an HOCL
  dynamic description of the workflow. The direct communication between the
  agents is assured by a persistent message broker. Two message brokers are
  currently supported: ActiveMQ and Kafka. The former is the default one. The
  latter provides a state recovery of agents in case of failure. More
  specifically, if some agent fails during execution, it can be restarted,
  without any further action from the user: kafka ensures that all the messages
  will be replayed so as to ensure the workflow will eventually recover. You will
  find more information on the bottow GinFlow layer in publication [FGCS]_.

.. [FGCS] Héctor Fernandez, Cédric Tedeschi, Thierry Priol: Rule-driven service
   coordination middleware for scientific applications. Future Generation Comp.
   Syst. 35: 1-13 (2014)


