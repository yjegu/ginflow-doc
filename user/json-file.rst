
JSON workflow description
=========================

As illustrated above, the simpler way to describe your workflow is to use
the JSON format. A GinFlow workflow is basically a JSON list of services,
preceeded by a name:

::

    {
      "name" : "workflow-name",
      "services": [
      ...
      ]

    }

Each service's description comprises the following possible fields:

::

    {
        "name"  : ["..."],
        "srv"   : ["..."],
        "in"    : ["..."],
        "src"   : ["..."],
        "dst"   : ["..."],
        "src_control" : ["..."],
        "dst_control" : ["..."],
        "alt"   : <int>,
        "sup"   : <int> 
    }

Let us briefly review these (all string) fields:

1. The :code:`name` field is a name you give to the service. It will be
   used as an identifier for references from the other services. :code:`MANDATORY`

2. The :code:`srv` field gives the command line to be executed when
   triggering this service. :code:`MANDATORY`

3. The :code:`src` field is a comma separated list of service identifiers
   denoting the incoming data dependencies of the service. All output
   from this list of services will be used as input to this service. 
   :code:`OPTIONAL, default = [""]`

4. The :code:`dst` field is a comma separated list of service identifiers
   to which send the output of this service. Note that the data
   dependencies are redundant: if there is a data dependency from *i* to
   *j*, :code:`i` appears in the :code:`src` field of service :code:`j`,
   and :code:`j` appears in the :code:`dst` field of service :code:`i`.
   :code:`OPTIONAL, default = [""]`

5. The :code:`src_control` and :code:`dst_control` fields are similar to
   :code:`src` and :code:`dst` fields respectively, except that they
   express *control* dependencies instead of *data* dependencies. A
   *control* dependency expresses that some service has to wait for the
   completion of another one to start, but will not take its output as
   input. :code:`OPTIONAL, default = [""]`
   
6. The :code:`alt` and :code:`sup` fields are exclusive and are related to the
   adaptation mechanism in play in GinFlow.
    
   * :code:`sup` indicates that the service is under supervision. It means that
     if the service returns and non zero status code, the adaptation mecanism
     will occur for the group the service belongs to. The integer value
     identifies the group of the service.
   * :code:`alt` indicates that the service is an alternative for the group
     identified by the given integer value.

