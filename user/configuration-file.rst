Configuration File
======================

By default the command line tries to load a file named *ginflow.yaml* in
the current directory. An alternative file may be specified using the *-c*
switch of the command line.  Here is the full specification of the options
you may find in the configuration file. 

Notice first the Kafka part.  Switching to a Kafka broker requires firstly
having a Kafka installation running (including a ZooKeeper server), and
secondly modifying the configuration file. The ``broker`` parameter must be
changed for ``Kafka``, and the Kafka and ZooKeeper servers fields need to be
filled.

If you have a Mesos instance running on top of your resources, and you
want GinFlow workers to be scheduled on your resources through it, you
need to fill the Mesos part of the configuration file (the four
``mesos.*`` parameters).

::

    ## Distribution

    distribution.home: # absolute path to the directory containing GinFlow JAR
    distribution.name: # GinFlow  binary e.g hocl-workflow-0.0.1-SNAPSHOT.jar

    #
    # Distributed parameters
    #
    ### -e mesos
    mesos.user         : # user to use to launch local invoker on mesos slaves
    mesos.master       : # adress:port of the mesos master
    mesos.resources.cpu: # cpu to use for running the local invoker e.g: "1"
    mesos.resources.mem: # memory ammount to use for running the local invoker e.g: "128"

    ### -e ssh
    ssh.machines   : # comma separated list of remote machines to use
    ssh.user       : # user login used to connect to the remote machines
    ssh.private_key: # path to the private key to use

    #
    # Communication parameters
    #
    broker: # broker to use, possible values are activeMQ, Kafka
    broker.standalone: # true if the broker is provided externally, 
                        false if an embedded broker shoud be started.
                        Default to false.

    ## ActiveMQ options
    activemq.broker.url: # endpoint of the activeMQ broker
                         # e.g: "failover://(tcp://localhost:61616)"

    ## Kafka
    kafka.bootstrap.servers: # comma separated list of bootstrap servers
                             # e.g: "localhost:9092"
    kafka.zookeeper.connect: # comma separated list of zookeeper nodes
                             # e.g: "localhost:2181"

    # hocl
    hocl.debug: # debug level of HOCL engine, possible value are "0", ... "9"

    # chaos
    chaos.threshold: # probability that a service agent fails
                     # e.g : "0.5"
    chaos.sleep:     # time before failure (ms)
                     # e.g : "10000"
