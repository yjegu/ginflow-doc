Command line Interface
======================

*launch*
^^^^^^^^^

We already met the *launch* subcommand. It allows you to launch a workflow. Let's
list its options:

::

    Usage: launch [options]
    Options:
      -c, --config
         Config file
         Default: ginflow.yaml

      -d, --debug
         Default: 0

      -e, --executor
         Executor to use, possible values are : central, mesos, ssh
         Default: central

      -h, --help
         Default: false

      -o, --option
         Executor options
         Syntax: -o key=value
         Default: {}

    * -w, --workflow
         Path to the json file containing the workflow definition

*clean* 
^^^^^^^

The *clean* subcommand can be used to clean service invokers on remote machines.
Usually the executor is responsible for cleaning the processes after the execution,
but in some rare cases (e.g: connection error to some nodes during the
deployment) you can use this subcommand to clean the remaining processes.


::

    Usage: clean [options]
    Options:
      -c, --config
         Config file
         Default: ginflow.yaml

      -d, --debug
         Default: 0

      -h, --help
         Default: false

      -o, --option
         Executor options
         Syntax: -okey=value
         Default: {}

    * -w, --workflow
         workflow id

*update*
^^^^^^^^^

The *update* subcommand can be used to trigger an adaptation of the running workflow.

::

   Usage: update [options]
    Options:
      -c, --config
         Config file
         Default: ginflow.yaml
      -d, --debug

         Default: 0
      -h, --help

         Default: false
      -o, --option
         Executor options
         Syntax: -okey=value
         Default: {}
    * -id, --uid
         workflow id
    * -w, --workflow
         Path to the json file containing the update to apply to the workflow 

.. _listen-reference-label:

*listen*
^^^^^^^^^

The *listen* subcommand allows you to create and launch a workflow via the graphical interface. To have a complete monitoring of the execution, the execution should be distributed (set executor to mesos or ssh). Let's list its options:

::

    Usage: listen [options]
    Options:
      -c, --config
         Config file
         Default: ginflow.yaml

      -d, --debug
         Default: 0

      -e, --executor
         Executor to use, possible values are : central, mesos, ssh
         Default: central

      -h, --help
         Default: false

      -o, --option
         Executor options
         Syntax: -o key=value
         Default: {}