Quick Start
============

This section will help you to go through the basics of running workflows
with GinFlow and is organised in four steps:

* Writing and executing your first workflow
* Changing the centralised executor to the distributed, SSH-based executor
* Creating a configuration file
* Using the environment variables

Your first workflow
^^^^^^^^^^^^^^^^^^^^

In this part you will learn how to create a simple workflow and run it on the central executor.

1. Download the |hocl-wf-jar| file from http://ginflow.inria.fr/download/.

2. Create your first workflow file *wf.json*. The following example
   creates a sequence of two services : the first outputs "!" and the
   second will output "?" concatenated to the output of the first service.

::

    {
      "name" : "test-workflow",
      "services": [{
        "name"  : ["1"],
        "srv"   : ["echo"],
        "in"    : ["!"],
        "dst"   : ["2"]
       }, {
         "name" : ["2"],
         "srv"  : ["echo"],
         "in"   : ["?"],
         "src"  : ["1"]
       }]
    }

* Launch it through the following command:

.. parsed-literal::

    java -jar |hocl-wf-jar| launch -w wf.json

* Check the result by having a look at the *wf.json.ginout* file generated
  at the end of the workflow execution. It contains the HOCL
  respresentation of the final state of the workflow. Without 
  worrying too much about HOCL code, notice that it is comprised of the
  final state of both services in some arbitrary order. Notice in
  particular the "out":... which contains the output of the service. In
  case of the second service, it is:

::

    .... "out":["! ?"] ...

Note finally that, :code:`"err":[...]` and :code:`"exit":[...]` artifacts
similarly contain the exit code and potential errors collected at run
time, respectively (from the execution of the echo command in this
particular case.)


Changing the executor to SSH
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

You executed your first workflow using the central (by default) executor.
In this section, you will learn how to use the SSH executor on your local
machine.

* Configure passwordless connection to your local machine.

:: 

  ssh-keygen -t rsa -f /tmp/ginkey -q -N ''
  cat /tmp/ginkey.pub >> ~/.ssh/authorized_keys


* Run the workflow using the SSH executor:

.. parsed-literal::

    java -jar |hocl-wf-jar| launch -w wf.json -e ssh -o ssh.private.key=/tmp/ginkey

::

    ssh.private.key=[path to your private key] # this private key will be used to
                                                initiate the passwordless SSH
                                                connection to the machines 
                                                specified by ssh.machines

Using a configuration file
^^^^^^^^^^^^^^^^^^^^^^^^^^^

GinFlow can read its options from a file called "ginflow.yaml" in the current
directory. In the previous case the content will be : 

::

    ssh.private.key: [path to your private key]

It simplifies the command line to the following (you only need to specify
which executor you use by using the :code:`-e` option:

.. parsed-literal::

        java -jar |hocl-wf-jar| launch -w wf.json -e ssh

Using environment variables
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 
Alternatively, GinFlow can read all these options from environment variables.
The mapping from the environment variable and an option is handled by the
following rule : 

::

  env var GINFLOW_X_Y <-> option x.y
  ex : 
  GINFLOW_SSH_PRIVATE_KEY <-> ssh.private.key

Thus the following command will launch the same workflow as before : 

::

  GINFLOW_SSH_PRIVATE_KEY=/tmp/ginkey java -jar hocl-workflow-X.Y.Z.jar launch -w wf.json -e ssh
